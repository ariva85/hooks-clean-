const branch = require("git-branch");
const exec = require("child_process").exec;
const fs = require("fs");
const moment = require("moment");
//const http = require("http");
const cmd = {
	pretty: "pretty-quick --staged --verbose",
	lint: "npm run eslint"
};
// const mock = [
//   { filename: "features/1.0_login.feature", flag: true },
//   { filename: "features/2.0_estates-search.feature", flag: true },
//   { filename: "features/3.0_test_a.feature", flag: true },
//   { filename: "features/4.0_test_b.feature", flag: true }
// ];

// const url = "http://www.mocky.io/v2/5bd855d5310000d42a474da9";
// let baseString = "cd test && npm run e2e-test --testcase ";

const cb = (error, stdout, stderr) => {
	console.log("result ->"); //", {error, stdout, stderr});
	console.group();
	console.log("error  ->", error);
	console.log("stdout ->", stdout);
	console.log("stderr ->", stderr);
	console.groupEnd();

	if (error) {
		const path = `./logs/err-${moment().format("YYYYMMDD_HHmmss")}.txt`;
		fs.writeFileSync(path, stdout, {flag: "w"}, function(err) {
			if (err) {
				console.error(err);
			}
		});
		process.exit(1);
	}
};

// const runTests = () => {
// 	return http
// 		.get(url, (resp) => {
// 			let data = "";

// 			resp.on("data", (chunk) => {
// 				data += chunk;
// 			});

// 			resp.on("end", () => {
// 				try {
// 					let array = JSON.parse(data);
// 					exec(
// 						baseString +
// 							array
// 								.filter((t) => t.flag)
// 								.map((t) => t.filename) // `features/${t.filename}.feature`
// 								.join(" "),
// 						cb
// 					);
// 				} catch (err) {
// 					process.exit(1);
// 				}
// 			});
// 		})
// 		.on("error", (err) => {
// 			console.log("Error: " + err.message);
// 			process.exit(1);
// 		});
// };

branch()
	.then((name) => {
		console.log(`branch -> ${name} |  hook -> ${process.argv[2]}`);
		if (name === "master") {
			if (process.argv[2] === "precommit") {
				exec(`${cmd.lint}`, cb);
			} else if (process.argv[2] === "prepush") {
				exec(cmd.lint, cb);
				//runTests();
			}
		} else if (name === "develop") {
			if (process.argv[2] === "precommit") {
				exec(`${cmd.pretty}`, cb);
			}
			if (process.argv[2] === "prepush") {
				exec(`${cmd.lint}`, cb);
			}
		}
	})
	.catch((err) => console.log(err));
