const seleniumServer = require("selenium-server");
const chromedriver = require("chromedriver");
const geckodriver = require("geckodriver");
const url = "http://RG4_WEB:90/"; //"http://10.110.13.215:18000";

require("nightwatch-cucumber")({
	cucumberArgs: [
		"--require",
		"step_definitions",
		"--format",
		"node_modules/cucumber-pretty",
		"--format",
		"json:reports/cucumber.json",
		"features",
		"optional-features"
	]
});

module.exports = {
	output_folder: "reports",
	custom_assertions_path: "",
	live_output: false,
	disable_colors: false,
	selenium: {
		start_process: true,
		server_path: seleniumServer.path,
		log_path: "",
		host: "127.0.0.1",
		port: 4444
	},
	test_settings: {
		default: {
			launch_url: url,
			selenium_port: 4444,
			selenium_host: "127.0.0.1",
			desiredCapabilities: {
				url,
				browserName: "chrome",
				javascriptEnabled: true,
				acceptSslCerts: true,
				chromeOptions: {
					args: ["incognito", "headless", "no-sandbox", "disable-gpu"]
				}
			},
			selenium: {
				cli_args: {
					"webdriver.chrome.driver": chromedriver.path
				}
			}
		},
		chrome: {
			desiredCapabilities: {
				url,
				browserName: "chrome",
				javascriptEnabled: true,
				acceptSslCerts: true
			},
			selenium: {
				cli_args: {
					"webdriver.chrome.driver": chromedriver.path
				}
			}
		},
		firefox: {
			desiredCapabilities: {
				url,
				browserName: "firefox",
				javascriptEnabled: true,
				marionette: true
			},
			selenium: {
				cli_args: {
					"webdriver.gecko.driver": geckodriver.path
				}
			}
		},
		local_chrome: {
			desiredCapabilities: {
				url: "http://localhost:8080",
				browserName: "chrome",
				javascriptEnabled: true,
				acceptSslCerts: true,
				chromeOptions: {
					args: ["incognito", "no-sandbox", "disable-gpu"]
				}
			},
			selenium: {
				cli_args: {
					"webdriver.chrome.driver": chromedriver.path
				}
			}
		}
	}
};
