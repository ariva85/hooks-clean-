Feature: RealGimm login

	Scenario Outline: Successful Login
		Given I visit RG4
		And I visit <tag> field and i write <type>
		And I visit <tag1> field and i write <type1>
		And I click on <outside-field>
		And I see <page0>
		And I click on <clicked>
		Then I see <title>

		Examples:

			| landing-page                       | page0         | tag        | type       | tag1       | type1     | outside-field | clicked         | page1  | title            |
			| "http://10.110.13.215:18000/login" | ".login-page" | "username" | "falzonev" | "password" | "siamrep" | "username"    | "submit-button" | "#app" | "[title=Logout]" |
