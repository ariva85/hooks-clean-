const { client } = require("nightwatch-cucumber");
const { Given, Then, When } = require("cucumber");

Given(/^I search for "([^"]*)"$/, function(typed) {
  return client
    .url(typed)
    .waitForElementVisible("body", 1000)
    .pause(1000);
});

Given(/^I visit RG4$/, function() {
  client
    .url(client.options.desiredCapabilities.url)
    .waitForElementVisible("body", 1000)
    .pause(1000);
});

Then(/^I see THE LIGHT$/, function(typed) {
  client
    .url(client.options.desiredCapabilities.url)
    .waitForElementVisible("body", 1000)
    .pause(1000);
});

Then(/^I see "([^"]*)"$/, function(typed) {
  return client.waitForElementVisible(typed, 3000);
});

Then(/^I see \"([^"]*)\" in \"([^"]*)\"$/, function(message, field) {
  return client.assert.containsText(field, message);
});

Then(/^I dont see an empty string in \"([^"]*)\"$/, function(field) {
  return client.expect.element(field).value.to.not.match(/^(?![\s\S])/);
});

Then(/^I visit "([^"]*)"$/, function(typed) {
  return client.assert.visible("[name=" + typed + "]");
});

Then(/^I visit \"([^"]*)\" field and i write \"([^"]*)\"$/, function(
  typedField,
  typedParameters
) {
  return client.assert
    .visible("[name=" + typedField + "]")
    .setValue("[name=" + typedField + "]", typedParameters)
    .pause(1000);
});

Then(/^I visit \"([^"]*)\" field and i write random text$/, function(
  typedField
) {
  var now = Date.now();
  client
    .clearValue("[name=" + typedField + "]")
    .setValue("[name=" + typedField + "]", now);
});

Then(/^I visit the \"([^"]*)\" field and i write \"([^"]*)\"$/, function(
  typedField,
  typedParameters
) {
  return client.assert
    .visible(typedField)
    .setValue(typedField, typedParameters)
    .pause(1000);
});

When(/^I visit \"([^"]*)\" field and i clear it$/, function(typedField) {
  return client.assert
    .visible("[name=" + typedField + "]")
    .clearValue("[name=" + typedField + "]")
    .pause(1000);
});

When(/^I click "([^"]*)"$/, function(typed) {
  return client.click(typed).pause(2000);
});

When(/^I wait$/, function() {
  return client.pause(5000);
});

When(/^I click on "([^"]*)"$/, function(typed) {
  return client.click("[name=" + typed + "]").pause(2000);
});

Then(/^I check \"([^"]*)\" is not \"([^"]*)\"$/, function(typed, typedCheck) {
  return client.assert.visible(typed + ":not(" + typedCheck + ")").pause(2000);
});

Then(/^I see \"([^"]*)\" field is not empty$/, function(field) {
  return client.expect.element(field).to.have.value.not.equals(null);
});
