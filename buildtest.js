const exec = require("child_process").exec;
const fs = require("fs");
const moment = require("moment");
const http = require("http");

const url = "http://www.mocky.io/v2/5bd855d5310000d42a474da9";
let baseString = "cd test && npm run e2e-test --testcase ";

const runTests = () => {
  return http
    .get(url, resp => {
      let data = "";

      resp.on("data", chunk => {
        data += chunk;
      });

      resp.on("end", () => {
        try {
          debugger;
          let array = JSON.parse(data);
          console.log(
            baseString +
              array
                .filter(t => t.flag)
                .map(t => t.filename)
                .join(" ")
          );
        } catch (err) {
          console.log("wtf", err);
          process.exit(1);
        }
      });
    })
    .on("error", err => {
      console.log("Error: " + err.message);
      process.exit(1);
    });
};

runTests();
