const branch = require("git-branch");
const exec = require("child_process").exec;

branch()
  .then(name => {
    console.log("branch -> ", name, " |  hook -> ", process.argv[2]);
    if (name === "master") {
      if (process.argv[2] === "precommit") {
        exec("npm run eslint", (e, stdout, stderr) => {
          console.log("callback master precommit", e, stdout, stderr);
        });
      } else if (process.argv[2] === "prepush") {
        exec("npm run eslint &&npm run test-nightwatch", (...args) => {
          console.log("callback master prepush", ...args);
        });
      }
    } else if (name === "develop") {
      if (process.argv[2] === "precommit") {
        exec("npm run eslint");
      }
      // else if (process.argv[2] === "prepush") {
      //   console.log("callback develop prepush");
      // }
    }
  })
  .catch(err => console.log(err));
